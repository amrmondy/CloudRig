from . import replace_rigify_ui, rig_types_ui

modules = [
    replace_rigify_ui,
    rig_types_ui
]